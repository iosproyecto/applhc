//
//  EventoViewController.swift
//  Gamify_Inicio
//
//  Created by Sonia Díaz on 16/8/17.
//  Copyright © 2017 EfectoApple. All rights reserved.
//

import UIKit
import FirebaseDatabase

class EventoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var eventoTableView: UITableView!

    var ref : FIRDatabaseReference?
    var eventosList: [Evento] = []
    //var eventosList: [String] = []
    var handle: FIRDatabaseHandle?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        eventoTableView.delegate = self
        eventoTableView.dataSource = self
        
        ref = FIRDatabase.database().reference().child("Evento")
        ref?.observe(FIRDataEventType.value, with: { (snapshot) in
            
            
            print("snapshot")
            print(snapshot.childrenCount)
            
            if snapshot.childrenCount > 0
            {
                self.eventosList.removeAll()
                
                for eventos in snapshot.children.allObjects as! [FIRDataSnapshot]{
                    let eventoObjeto = eventos.value as? [String: AnyObject]
                    
                    let eventoNombre = eventoObjeto?["nombre"]
                    let eventoFecha = eventoObjeto?["fecha"]
                    let eventoImagen = eventoObjeto?["imagen"]
                    let evento = Evento(nombre:eventoNombre as! String?, fecha: eventoFecha as! String?, imagen: eventoImagen as! String?)
                    
                    self.eventosList.append(evento)
                }
                
                self.eventoTableView.reloadData()
                
            }
            else{
                print("No entro a snapshot")
            }
            
        })
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //let cell = UITableViewCell(style : .default, reuseIdentifier : "cell" )
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! EventoTableViewCell
        let eventoT: Evento
        eventoT = eventosList[indexPath.row]
        print("ddd", eventoT.fecha!)
        cell.nombreEventoLabel.text = eventoT.nombre
        cell.fechaLabel.text = eventoT.fecha
        cell.direccionLabel.text = eventoT.imagen
        
        
        //  cell.fechaLabel?.text = eventosList[indexPath.row]
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return eventosList.count
    }
    
    


}
