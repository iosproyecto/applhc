//
//  Evento.swift
//  Gamify_Inicio
//
//  Created by Sonia Díaz on 16/8/17.
//  Copyright © 2017 EfectoApple. All rights reserved.
//

import UIKit

class Evento: NSObject{
    
    var nombre: String?
    var fecha: String?
    var imagen: String?
    
    init(nombre: String? , fecha: String?, imagen: String?) {
        
        self.nombre = nombre
        self.fecha = fecha
        self.imagen = imagen
    }
    
}
