//
//  EventoTableViewCell.swift
//  Gamify_Inicio
//
//  Created by Sonia Díaz on 16/8/17.
//  Copyright © 2017 EfectoApple. All rights reserved.
//

import UIKit

class EventoTableViewCell: UITableViewCell {

    @IBOutlet weak var nombreEventoLabel: UILabel!
    @IBOutlet weak var logoEventoImageView: UIImageView!
    @IBOutlet weak var direccionLabel: UILabel!
    @IBOutlet weak var fechaLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
 
